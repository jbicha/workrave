Source: workrave
Section: gnome
Priority: optional
Maintainer: Francois Marier <francois@debian.org>
Uploaders: Jordi Mallach <jordi@debian.org>
Build-Depends: autoconf-archive,
               debhelper (>= 11),
               docbook-utils,
               gobject-introspection (>= 0.6.7),
               gsettings-desktop-schemas-dev,
               intltool,
               libdbus-glib-1-dev (>= 0.84),
               libdbusmenu-glib-dev (>= 0.1.1),
               libdbusmenu-gtk3-dev (>= 0.3.95),
               libgdome2-dev,
               libgirepository1.0-dev (>= 0.6.7),
               libglib2.0-0 (>= 2.30),
               libglibmm-2.4-dev (>= 2.28.0),
               libgstreamer1.0-dev,
               libgtk-3-dev (>= 3.0.0),
               libgtkmm-3.0-dev (>= 3.0.0),
               libindicator3-dev (>= 0.3.19),
               libmate-panel-applet-dev,
               libpanel-applet-dev,
               libpulse-dev (>= 0.9.5),
               libsigc++-2.0-dev (>= 2.2.4.2),
               libxi-dev,
               libxmu-dev,
               libxss-dev,
               libxtst-dev,
               python-cheetah,
               xfce4-panel-dev,
               xmlto
Build-Conflicts: libgstreamer0.10-dev
Standards-Version: 4.2.1
Homepage: http://www.workrave.org/
Vcs-git: https://salsa.debian.org/debian/workrave.git
Vcs-Browser: https://salsa.debian.org/debian/workrave

Package: workrave
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: workrave-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: gnome-panel, gnome-shell, xfce4-panel
Description: Repetitive Strain Injury prevention tool
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.

Package: workrave-data
Architecture: all
Depends: ${misc:Depends}
Breaks: workrave (<< 1.9.909)
Replaces: workrave (<< 1.9.909)
Description: Repetitive Strain Injury prevention tool (data files)
 Workrave is a program that assists in the recovery and prevention of
 Repetitive Strain Injury (RSI). The program frequently alerts you to
 take micro-pauses, rest breaks and restricts you to your daily limit.
 .
 It includes a system tray applet that works with GNOME and KDE
 and has network capabilities to monitor your activity even if
 switching back and forth between different computers is part of your
 job.
 .
 Workrave offers many more configuration options than other similar
 tools.
 .
 This package contains the required data files common to all architectures.
